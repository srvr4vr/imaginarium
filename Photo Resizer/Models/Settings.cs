﻿using System;
using System.Collections.Generic;
using System.IO;
using ImageResizerCore;
using Newtonsoft.Json;

namespace Photo_Resizer.Models
{
    [Serializable]
    public class Settings: ISettingsService
    {
        private Dictionary<int, ImageSize> _sizeDict;
        private string _login;
        private string _password;
        private string _watermarkText;
        private string _hostAdress;
        private int _watermarkOpacity;

        public string HostAdress
        {
            get => _hostAdress;
            set { _hostAdress = value; Save(); }
        }

        public string WatermarkText
        {
            get => _watermarkText;
            set { _watermarkText = value; Save(); }
        }

        public int WatermarkOpacity
        {
            get => _watermarkOpacity;
            set { _watermarkOpacity = value; Save();}
        }

        public string Login
        {
            get => _login;
            set { _login = value; Save(); }
        }

        public string Password
        {
            get => _password;
            set { _password = value; Save();}
        }

        public IDictionary<int, ImageSize> SizeDict => _sizeDict;

        private static readonly string SettingsFile =
            Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Imaginarium\\settings.json";

        public void AddResolution(ImageSize imageSize)
        {
            _sizeDict.Add(SizeDict.Count, imageSize);
            Save();
        }

        public Settings()
        {
            _sizeDict = new Dictionary<int, ImageSize>();
        }

        public void Load()
        {
            if (!File.Exists(SettingsFile))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(SettingsFile) ?? throw new InvalidOperationException());
                _login = _password = _watermarkText=string.Empty;
                SizeDict[99] = new ImageSize(0, 0, "Оставить без изменений");
                Save();
                return;
            }
            var json = File.ReadAllText(SettingsFile);
            var tmp = JsonConvert.DeserializeObject<Settings>(json);
            _sizeDict = (Dictionary<int, ImageSize>) tmp.SizeDict;
            Login = tmp.Login;
            Password = tmp.Password;
            WatermarkText = tmp.WatermarkText;
            WatermarkOpacity = tmp.WatermarkOpacity;
            HostAdress = tmp.HostAdress;
        }

        public void Save() => File.WriteAllText(SettingsFile, JsonConvert.SerializeObject(this));
    }
}
