﻿using System.Collections.Generic;
using Photo_Resizer.Interfaces;

namespace Photo_Resizer.Models
{
    public class ServerItem
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }

        public ServerItem Parent { get; set; }
        public List<ServerItem> ChildList { get; set; }

        public ServerItem(IBitrixItem ex) : this()
        {
            Id = ex.Id;
            Name = ex.Name;
            ParentId = ex.ParentId;
        }

        public ServerItem()
        {
            ChildList = new List<ServerItem>();
        }

        public override string ToString() => $"[{Id}]: {Name}";
    }
}