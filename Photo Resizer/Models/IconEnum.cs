﻿namespace Photo_Resizer.Models
{
    public enum IconEnum
    {
        Harddisk,
        Folder,
        Sd,
        ServerNetwork
    }
}