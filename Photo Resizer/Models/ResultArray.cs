﻿using System;
using System.Collections.Generic;
using System.Linq;
using Catel.IoC;
using ImageResizerCore;

namespace Photo_Resizer.Models
{
    public class ResultArray: ICloneable
    {
        public IEnumerable<ImageModel> ImageList { get; set; }
        public ProgramModeEnum ProgramModeEnum { get; set; }
        public bool IsUseWatermark { get; set; }
        public ImageSize ResizeResolution { get; set; }
        public string Target { get; set; }

        public ResultArray(IEnumerable<ImageModel> lst)
        {
            ImageList = lst;
            ResizeResolution = this.GetServiceLocator().ResolveType<ISettingsService>().SizeDict.FirstOrDefault().Value;
            IsUseWatermark = true;
        }

        public ResultArray(ResultArray ra)
        {
            ImageList = ra.ImageList.ToList();
            ProgramModeEnum = ra.ProgramModeEnum;
            IsUseWatermark = ra.IsUseWatermark;
            ResizeResolution = ra.ResizeResolution;
            Target =(string) ra.Target?.Clone();
        }

        public object Clone() => new ResultArray(this);
    }
}