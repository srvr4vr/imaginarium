﻿using System.Drawing;
using System.IO;
using Catel.IoC;
using ImageResizerCore;

namespace Photo_Resizer.Models
{
    public class ImageModel
    {
        public string FilePath { get; }
        public string Name { get; }

        public Bitmap GetResizedBitmap(ImageSize size) =>
            size.IsZero() ? new Bitmap(FilePath) : _resizer.ResizeImage(FilePath, size);

        public Bitmap GetResizedBitmap(ImageSize size, string watermark, int opacity)
        {
            var bmp = size.IsZero() ? new Bitmap(FilePath) : _resizer.ResizeImage(FilePath, size);
            return bmp.StampWatermarkHorizontal(watermark, opacity);
        }

        public Bitmap GetResizedBitmap(Bitmap watermark)
        {
            var bmp =  _resizer.ResizeImage(FilePath, new ImageSize(1500,0));
            return bmp.StampWatermark(watermark);
        }

        private readonly IImageResizer _resizer;

        public ImageModel(string filePath)
        {
            _resizer=this.GetDependencyResolver().Resolve<IImageResizer>();
            FilePath = filePath;
            Name = Path.GetFileName(filePath);
        }

        public static bool IsImage(string path)
        {
            var extension = Path.GetExtension(path)?.ToLower();
            return ExtensionIsImage(extension);
        }

        private static bool ExtensionIsImage(string extension)
        {
            extension = extension.ToLower();
            return extension.Equals(".jpg") || extension.Equals(".bmp") || extension.Equals(".png") ||
                   extension.Equals(".jpeg");
        }
    }
}
