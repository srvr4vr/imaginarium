﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ImageResizerCore;
using Newtonsoft.Json;
using Photo_Resizer.Annotations;
using Photo_Resizer.Interfaces;

namespace Photo_Resizer.Models
{
    [UsedImplicitly]
    public class Bitrix : IBitrix
    {
        private readonly ISettingsService _settings;
        private Uri ToolsAdress => new Uri(new Uri(_settings.HostAdress), "/tools");

        public Bitrix(ISettingsService settings)
        {
            _settings = settings;
            _wc = new WebClient { Encoding = Encoding.UTF8 };
            var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{_settings.Login}:{_settings.Password}"));
            _wc.Headers[HttpRequestHeader.Authorization] = $"Basic {credentials}";
        }

        ~Bitrix() => _wc.Dispose();

        private readonly WebClient _wc;

        private static string ImageToBase64(Image image)
        {
            using (var ms = new MemoryStream())
            {
                var encoder = ImageResizer.GetJpegCodecInfo();
                var encoderParams = ImageResizer.GetJpegEncoderParams(85L);
                image.Save(ms, encoder, encoderParams);
                var imageBytes = ms.ToArray();
                image.Dispose();
                return Convert.ToBase64String(imageBytes);
            }
        }

        public async Task<string> UploadToPhotogalery64(string id, string fileName, Bitmap image) =>
            await UploadFile64(image, new Uri($"{ToolsAdress}/pg.upload.base64.php?id={id}&name={fileName}"));

        public async Task<string> UploadToMedialib64(string id, string fileName, Bitmap image) =>
            await UploadFile64(image, new Uri($"{ToolsAdress}/ml.upload.base64.php?id={id}&name={fileName}"));

        private async Task<string> UploadFile64(Image image, Uri url)
        {
            try
            {
                var base64Image = ImageToBase64(image);
                var response =
                    await _wc.UploadValuesTaskAsync(url, new NameValueCollection {{"myImageData", base64Image}});
                return Encoding.GetEncoding(1251).GetString(response);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return string.Empty;
            }
        }

        public async Task<List<ServerItem>> GetMedialibList()
        {
            try
            {
                var uri = new Uri($"{ToolsAdress}/ml.getSections.php");
                var str = await _wc.DownloadStringTaskAsync(uri);
                return JsonConvert.DeserializeObject<List<MediaGaleryItem>>(str).Select(x => new ServerItem(x))
                    .ToList();
            }
            catch (Exception e )
            {
                Debug.WriteLine(e.Message);
                return new List<ServerItem>();
            }
        }

        public async Task<List<ServerItem>> GetPhotogaleyList()
        {
            try
            {
                var uri = new Uri($"{ToolsAdress}/pg.getSections.php");
                var str = await _wc.DownloadStringTaskAsync(uri);
                return JsonConvert.DeserializeObject<List<IblockItem>>(str).Select(x => new ServerItem(x)).ToList();
            }
            catch (Exception e)
            {
                return new List<ServerItem>();
            }
        }

        public async Task<OperationResult> CreateMedialibraryFolder(string name, string parentId)
        {
            try
            {
                var str = await _wc.DownloadStringTaskAsync(
                    new Uri($"{ToolsAdress}/ml.createSection.php?id={parentId}&name={name}"));
                return JsonConvert.DeserializeObject<OperationResult>(str);
            }
            catch (Exception)
            {
                return new OperationResult {Result = "error",Code=string.Empty};
            }
        }

        public async Task<OperationResult> CreatePhotogaleryFolder(string name, string parentId)
        {
            try
            {
                var str = await _wc.DownloadStringTaskAsync(
                    new Uri($"{ToolsAdress}/pg.createSection.php?id={parentId}&name={name}"));
                return JsonConvert.DeserializeObject<OperationResult>(str);
            }
            catch (Exception)
            {
                return new OperationResult {Result = "error", Code = string.Empty};
            }
        }
    }
}