﻿namespace Photo_Resizer.Models
{
    public enum ProgramModeEnum
    {
        Medialibrary,
        Photogalery,
        Disk
    }
}