﻿using Newtonsoft.Json;
using Photo_Resizer.Interfaces;

namespace Photo_Resizer.Models
{
    public class MediaGaleryItem : IBitrixItem
    {

        [JsonProperty("ID")]
        public string IdString { get; set; }

        public int Id => int.Parse(IdString);
        [JsonProperty("NAME")]
        public string Name { get; set; }

        [JsonProperty("PARENT_ID")]
        private string _parentIdString;

        public string ParentIdString
        {
            get => _parentIdString;
            set => _parentIdString = string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public int ParentId
        {
            get => string.IsNullOrWhiteSpace(ParentIdString) ? 0 : int.Parse(ParentIdString);

            set => ParentIdString = value.ToString();
        }


        public override string ToString() => $"[{Id}]: {Name} родитель {ParentId}";

        public MediaGaleryItem()
        {
            ParentIdString = "0";
            ParentId = 0;
        }

    }
}