﻿using Newtonsoft.Json;

namespace Photo_Resizer.Models
{
    public class MediaLibResult
    {
        [JsonProperty("Result")]
        public string Result { get; set; }
        [JsonProperty("thumpUrl")]
        public string ThumpUrl { get; set; }
        [JsonProperty("url")]
        public string Url { get; set; }
    }
}
