﻿using Newtonsoft.Json;

namespace Photo_Resizer.Models
{
    public class OperationResult
    {
        [JsonProperty("result")]
        public string Result;

        [JsonProperty("code")]
        public string Code;
    }
}