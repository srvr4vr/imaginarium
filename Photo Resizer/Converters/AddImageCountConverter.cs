﻿using System;
using System.Globalization;
using System.Windows.Data;
using Photo_Resizer.Tools;
using Photo_Resizer.ViewModels;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(FileSystemEntityViewModel), typeof(int))]
    public class AddImageCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            var v = value as FileSystemEntityViewModel;
            return v?.GetImageCount();
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}