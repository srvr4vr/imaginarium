﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(int), typeof(bool))]
    public class IntToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            var result = false;
            if (value is int i)
            {
                result = i > 0;
            }
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}