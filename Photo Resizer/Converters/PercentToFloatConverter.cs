﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(int), typeof(double))]
    public class PercentToFloatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            // ReSharper disable once PossibleLossOfFraction
            CultureInfo culture) => System.Convert.ToInt32(value) / 100d;

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}