﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(int), typeof(bool))]
    class HProcentToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {

            return value != null && (int)value != 100;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
