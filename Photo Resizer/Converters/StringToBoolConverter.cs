﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(string), typeof(bool))]
    public class StringToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            var v = value as string;
            return !string.IsNullOrWhiteSpace(v);
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}