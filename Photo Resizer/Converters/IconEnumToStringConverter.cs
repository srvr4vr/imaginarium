﻿using System;
using System.Globalization;
using System.Windows.Data;
using Photo_Resizer.Models;

namespace Photo_Resizer.Converters
{
    [ValueConversion(typeof(IconEnum), typeof(string))]
    public class IconEnumToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value == null) return string.Empty;
            var v = (IconEnum)value;
            return v.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}