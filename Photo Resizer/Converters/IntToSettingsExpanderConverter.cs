﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Photo_Resizer.Converters
{
    
    [ValueConversion(typeof(int), typeof(Visibility))]
    public class IntToSettingsExpanderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return $"Доступные разрешения: {value}";
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}