﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Photo_Resizer.Annotations;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    /// <inheritdoc />
    /// <summary>
    /// TreeItem view model.
    /// </summary>
    public class TreeItemViewModel : INotifyPropertyChanged //ViewModelBase
    {
        
        private int _id;
        private string _name;
        private ObservableCollection<TreeItemViewModel> _childList;

        public int Id
        {
            get => _id;
            set { _id = value; OnPropertyChanged(); }
        }

        public string Name
        {
            get => _name;
            set { _name = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<TreeItemViewModel> ChildList
        {
            get => _childList;
            set { _childList = value;
                OnPropertyChanged();
            }
        }
        #region Constructors

        public TreeItemViewModel(ServerItem item): this()
        {
            Id = item.Id;
            Name = item.Name;
        }

        public TreeItemViewModel()
        {
            ChildList = new ObservableCollection<TreeItemViewModel>();
        }
        #endregion

        public override string ToString() => $"[{Id}]: {Name}";

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}