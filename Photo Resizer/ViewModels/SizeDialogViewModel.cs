﻿using System.Collections.Generic;
using Catel.Data;
using Catel.MVVM;
using ImageResizerCore;
using MaterialDesignThemes.Wpf;

namespace Photo_Resizer.ViewModels
{
    public class SizeDialogViewModel: ViewModelBase
    {
        #region Ok command
        private Command _okCommand;

        /// <summary>
        /// Gets the Ok command.
        /// </summary>
        public Command OkCommand => _okCommand ?? (_okCommand = new Command(Ok, IsCanPressOk));

        /// <summary>
        /// Method to invoke when the Ok command is executed.
        /// </summary>
        private void Ok()
        {
            var isHeightParced = int.TryParse(Height, out var height);
            if (!isHeightParced) height = 0;
            var isWidthParced = int.TryParse(Width, out var width);
            if (!isWidthParced) width = 0;

            DialogHost.CloseDialogCommand.Execute(
                new KeyValuePair<int, ImageSize>(_id, new ImageSize(width, height)), null);
        }

        #endregion

        private bool IsCanPressOk()
        {
            var h = int.TryParse(Height, out var height);
            
            var w = int.TryParse(Width, out var width);
            return h || w && (height > 0 || width > 0);
        }

        #region Height property

        /// <summary>
        /// Gets or sets the Height value.
        /// </summary>
        public string Height
        {
            get => GetValue<string>(HeightProperty);
            set => SetValue(HeightProperty, value);
        }

        /// <summary>
        /// Height property data.
        /// </summary>
        public static readonly PropertyData HeightProperty = RegisterProperty("Height", typeof(string));

        #endregion

        #region Width property

        /// <summary>
        /// Gets or sets the Width value.
        /// </summary>
        public string Width
        {
            get => GetValue<string>(WidthProperty);
            set => SetValue(WidthProperty, value);
        }

        /// <summary>
        /// Width property data.
        /// </summary>
        public static readonly PropertyData WidthProperty = RegisterProperty("Width", typeof(string));

        #endregion

        private readonly int _id;

        public SizeDialogViewModel()
        {
            _id = -1;
        }

        public SizeDialogViewModel(KeyValuePair<int, ImageSize> item): this()
        {
            _id = item.Key;
            Height = item.Value.Height.ToString();
            Width = item.Value.Width.ToString();
        }
    }
}
