﻿using System.Collections.Generic;
using System.Windows;
using Catel.Data;
using Catel.MVVM;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public class HtmlResultViewModel: ViewModelBase
    {
        #region Result property

        /// <summary>
        /// Gets or sets the Result value.
        /// </summary>
        public IEnumerable<MediaLibResult> Result
        {
            get => GetValue<IEnumerable<MediaLibResult>>(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        /// <summary>
        /// Result property data.
        /// </summary>
        public static readonly PropertyData ResultProperty = RegisterProperty("Result", typeof(IEnumerable<MediaLibResult>));

        #endregion

        public HtmlResultViewModel()
        {
            
        }
        #region Html property

        /// <summary>
        /// Gets or sets the Html value.
        /// </summary>
        public string Html => GetHtml();
        
        private string GetHtml()
        {
            var result = "<div>\r\n";
            foreach (var mediaLibResult in Result)
            {
                if (mediaLibResult.Result != "success") continue;
                result += "<div class=\"imaginarium-cont\">";
                result += $"<a href=\"{mediaLibResult.Url}\" rel=\"lightbox[1]\">";
                result += $"<img class=\"imaginagium\" src=\"{mediaLibResult.ThumpUrl}\"/>";
                result += "</a></div>\r\n";
            }
            result += "</div>";
            Clipboard.SetText(result);
            return result;
        }

        #endregion

        public HtmlResultViewModel(IEnumerable<MediaLibResult> mediaLibResults)
        {
            Result = mediaLibResults;
        }
    }
}