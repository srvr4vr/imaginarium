﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Catel.Collections;
using Photo_Resizer.Annotations;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public sealed class FileSystemEntityViewModel : INotifyPropertyChanged
    {
        private string _name;
        private string _fullName;
        private IconEnum _icon;
        private ObservableCollection<FileSystemEntityViewModel> _childs;
        private bool _isExpanded;

        public FileSystemEntityViewModel(string name, string fullName, IconEnum icon): this()
        {
            Icon = icon;
            FullName = fullName;
            Name = name;
            Childs.Add(new FileSystemEntityViewModel());
        }

        public FileSystemEntityViewModel()
        {
            Childs = new FastObservableCollection<FileSystemEntityViewModel>();
        }

        private IEnumerable<FileSystemEntityViewModel> AddSubFolders()
        {
            try
            {
                var dirs = Directory.GetDirectories(FullName);
                if (dirs.Length < 1) return new List<FileSystemEntityViewModel>();

                var dirVms = dirs
                    .Select(x =>
                        new FileSystemEntityViewModel(Path.GetFileName(x), Path.Combine(FullName, x), IconEnum.Folder))
                    .ToArray();
                return dirVms;
            }
            catch (Exception )
            {
                return new List<FileSystemEntityViewModel>();
            }
        }

        [UsedImplicitly]
        public bool IsExpanded
        {
            get => _isExpanded;
            set
            {
                _isExpanded = value;
                OnPropertyChanged();
                if (Childs.Any(x => string.IsNullOrEmpty(x.Name)))
                {
                    Childs.Clear();
                    Childs.AddRange(AddSubFolders());
                }
            }
        }

        public ObservableCollection<FileSystemEntityViewModel> Childs
        {
            get => _childs;
            set
            {
                _childs = value;
                OnPropertyChanged();
            }
        }

        public string FullName
        {
            get => _fullName;
            set
            {
                _fullName = value;
                OnPropertyChanged();
            }
        }

        public string Name
        {
            get => _name;
            set { _name = value;
                OnPropertyChanged();
            }
        }

        public IconEnum Icon
        {
            get => _icon;
            set
            {
                _icon = value;
                OnPropertyChanged();
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}