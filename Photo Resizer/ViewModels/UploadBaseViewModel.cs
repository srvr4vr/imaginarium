﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catel.Data;
using Catel.IoC;
using Catel.MVVM;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public abstract class UploadBaseViewModel: ViewModelBase
    {

        public UploadBaseViewModel() { }

        #region TotalCount property
        /// <summary>
        /// Gets or sets the TotalCount value.
        /// </summary>
        public int TotalCount
        {
            get => GetValue<int>(TotalCountProperty);
            set => SetValue(TotalCountProperty, value);
        }

        /// <summary>
        /// TotalCount property data.
        /// </summary>
        public static readonly PropertyData TotalCountProperty = RegisterProperty("TotalCount", typeof(int));

        #endregion
        #region Result property

        /// <summary>
        /// Gets or sets the Result value.
        /// </summary>
        protected ResultArray Result
        {
            get => GetValue<ResultArray>(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        /// <summary>
        /// Result property data.
        /// </summary>
        public static readonly PropertyData ResultProperty = RegisterProperty("Result", typeof(ResultArray));

        #endregion

        #region Count property

        /// <summary>
        /// Gets or sets the Count value.
        /// </summary>
        public int Count
        {
            get => GetValue<int>(CountProperty);
            set => SetValue(CountProperty, value);
        }

        /// <summary>
        /// Count property data.
        /// </summary>
        public static readonly PropertyData CountProperty = RegisterProperty(nameof(Count), typeof(int));

        #endregion

        #region Progress property

        /// <summary>
        /// Gets or sets the Progress value.
        /// </summary>
        public int Progress => (int)(Count / (float)TotalCount * 100);

        #endregion

        protected override void OnPropertyChanged(AdvancedPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.PropertyName.Equals(nameof(Count)))
            {
                RaisePropertyChanged(nameof(Progress));
            }

            if (e.PropertyName.Equals(nameof(TotalCount)))
            {
                RaisePropertyChanged(nameof(Progress));
            }
        }
        protected  CancellationTokenSource Cts;

        protected readonly ISettingsService Settings;

        protected UploadBaseViewModel(ResultArray ra, ISettingsService settings)
        {
            Settings = settings;
            Cts = new CancellationTokenSource();
            Result = ra;
            TotalCount = Result.ImageList.Count();
            Count = 0;
        }

        #region Cancel command

        // ReSharper disable once InconsistentNaming
        protected Command _cancelCommand;

        /// <summary>
        /// Gets the Cancel command.
        /// </summary>
        public Command CancelCommand => _cancelCommand ?? (_cancelCommand = new Command(Cancel));

        /// <summary>
        /// Method to invoke when the Cancel command is executed.
        /// </summary>
        private async void Cancel()
        {
            Cts.Cancel();
            await Task.Delay(500);
            DialogHost.CloseDialogCommand.Execute(null, null);
        }
        #endregion

        public abstract void Start(CancellationToken cancellationToken);
    }
}