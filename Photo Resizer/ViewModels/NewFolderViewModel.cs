﻿using Catel.Data;
using Catel.MVVM;

namespace Photo_Resizer.ViewModels
{
    public class NewFolderViewModel: ViewModelBase
    {
        #region FolderTitle property

        /// <summary>
        /// Gets or sets the Title value.
        /// </summary>
        public string FolderTitle
        {
            get => GetValue<string>(FolderTitleProperty);
            set => SetValue(FolderTitleProperty, value);
        }

        /// <summary>
        /// Title property data.
        /// </summary>
        public static readonly PropertyData FolderTitleProperty = RegisterProperty("FolderTitle", typeof(string));

        #endregion

        public NewFolderViewModel() { }


        public NewFolderViewModel(string name)
        {
            FolderTitle = $"Создать папку в {name}";
        }
    }
}