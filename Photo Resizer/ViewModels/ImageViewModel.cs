﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Photo_Resizer.Annotations;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public class ImageViewModel : INotifyPropertyChanged
    {
        public string FilePath => Model.FilePath;
        public string Name => Model.Name;
        private BitmapImage _thumbian;
        private bool _isLoading;
        private ImageModel _model;

        public ImageViewModel(string file)
        {
            Model = new ImageModel(file);
            IsLoading = true;
        }

        public ImageViewModel(ImageModel fileModel)
        {
            IsLoading = true;
            Model = fileModel;
        }

        public bool IsLoading
        {
            get => _isLoading;
            set { _isLoading = value; OnPropertyChanged(); }
        }

        public async Task UpdateImage()
        {
            Thumbian = await Generate(FilePath, 10);
            Debug.WriteLine("loading false");
            IsLoading = false;
            LoadDetail();
        }

        private async void LoadDetail()
        {
            Thumbian = await Generate(FilePath, 100);
        }

        private static Task<BitmapImage> Generate(string file, int scale)
        {
            return Task.Run(() =>
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = new Uri(file);
                image.DecodePixelWidth = scale;
                image.EndInit();
                image.Freeze(); // important
                return image;
            });
        }

        public ImageModel Model
        {
            get => _model;
            set { _model = value; OnPropertyChanged(); }
        }

        public BitmapImage Thumbian
        {
            get => _thumbian;
            set { _thumbian = value; OnPropertyChanged(); Debug.WriteLine("Thumbian load");}
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}