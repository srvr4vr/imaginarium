﻿using System.Drawing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using ImageResizerCore;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public sealed class SaveFileToDiskViewModel: UploadBaseViewModel
    {
        public SaveFileToDiskViewModel(ResultArray ra, ISettingsService settings) : base(ra, settings)
        {
            
            Start(Cts.Token);
        }

        public override async void Start(CancellationToken cancellationToken)
        {
            await Task.Delay(1000, cancellationToken);
            try
            {
                foreach (var img in Result.ImageList)
                {
                    if (cancellationToken.IsCancellationRequested)
                        return;
                    await Task.Run(() =>
                    {
                        using (var bmp = Result.IsUseWatermark
                            // ? img.GetResizedBitmap(Result.ResizeResolution, Settings.WatermarkText, (int)(Settings.WatermarkOpacity * 2.55))
                            //: img.GetResizedBitmap(Result.ResizeResolution))
                            ? img.GetResizedBitmap(Properties.Resources.watermark)
                            : img.GetResizedBitmap(new ImageSize(1500, 0)))
                        {
                            var encoder = ImageResizer.GetJpegCodecInfo();
                            var encoderParams = ImageResizer.GetJpegEncoderParams(85L);
                            bmp?.Save(Path.Combine(Result.Target, img.Name), encoder, encoderParams);
                        }
                    }, cancellationToken);
                    Count++;

                }
                DialogHost.CloseDialogCommand.Execute(null, null);
            }
            catch { };
        }
    }
}