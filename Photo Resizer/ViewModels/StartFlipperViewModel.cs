﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Catel.Collections;
using Catel.Data;
using Catel.Messaging;
using Catel.MVVM;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public class StartFlipperViewModel: ViewModelBase
    {
        #region IsStartCardFlipped property
        /// <summary>
        /// Gets or sets the IsStartCardFlipped value.
        /// </summary>
        public bool IsStartCardFlipped
        {
            get => GetValue<bool>(IsStartCardFlippedProperty);
            set => SetValue(IsStartCardFlippedProperty, value);
        }

        /// <summary>
        /// IsStartCardFlipped property data.
        /// </summary>
        public static readonly PropertyData IsStartCardFlippedProperty = RegisterProperty("IsStartCardFlipped", typeof(bool));

        #endregion

        public StartFlipperViewModel()
        {
            Debug.WriteLine("StartFlipperViewModel - instantient");
            IsStartCardFlipped = false;
        }

        #region FileSystemRoot property

        /// <summary>
        /// Gets or sets the FileSystemRoot value.
        /// </summary>
        public FileSystemEntityViewModel FileSystemRoot
        {
            get => GetValue<FileSystemEntityViewModel>(FileSystemRootProperty);
            set => SetValue(FileSystemRootProperty, value);
        }

        /// <summary>
        /// FileSystemRoot property data.
        /// </summary>
        public static readonly PropertyData FileSystemRootProperty = RegisterProperty("FileSystemRoot", typeof(FileSystemEntityViewModel));

        #endregion
        private readonly IMessageMediator _messageService;


        public StartFlipperViewModel(IMessageMediator messageService)
        {
            _messageService = messageService;
            Fill();
        }
        
        public void Fill()
        {
            var root = new FileSystemEntityViewModel();
            var drives = DriveInfo.GetDrives().Where(x =>
                x.DriveType == DriveType.Fixed || x.DriveType == DriveType.Removable ||
                x.DriveType == DriveType.Network);
            var disksVms = new List<FileSystemEntityViewModel>();
            foreach (var drive in drives)
            {
                IconEnum icon;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (drive.DriveType)
                {
                    case DriveType.Removable:
                        icon = IconEnum.Sd;
                        break;
                    case DriveType.Network:
                        icon = IconEnum.ServerNetwork;
                        break;
                    default:
                        icon = IconEnum.Harddisk;
                        break;
                }
                disksVms.Add(new FileSystemEntityViewModel(drive.Name, drive.Name, icon));

            }
            FileSystemRoot = root;
            root.Childs.AddRange(disksVms);
        }

        #region GoNext command

        private Command<FileSystemEntityViewModel> _goNextCommand;

        /// <summary>
        /// Gets the GoNext command.
        /// </summary>
        public Command<FileSystemEntityViewModel> GoNextCommand =>
            _goNextCommand ?? (_goNextCommand = new Command<FileSystemEntityViewModel>(GoNext));

        /// <summary>
        /// Method to invoke when the GoNext command is executed.
        /// </summary>
        private void GoNext(FileSystemEntityViewModel selectedFolder)
        {
            var images = Directory.GetFiles(selectedFolder.FullName).Where(ImageModel.IsImage)
                .Select(x => new ImageViewModel(x));
            _messageService.SendMessage(images, "openfolder");
            IsStartCardFlipped = false;
            Fill();
        }
        #endregion
    }
}