﻿using System.Collections.Generic;
using Catel.Collections;
using Catel.Data;
using Catel.IoC;
using Catel.MVVM;
using ImageResizerCore;
using Photo_Resizer.Annotations;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    /// <summary>
    /// ViewModelName view model.
    /// </summary>
    public class ChooseWayViewModel : ViewModelBase
    {
        #region Fields

        #endregion
        
        #region Constructors

        [UsedImplicitly]
        public ChooseWayViewModel() { }

        public ChooseWayViewModel(ResultArray ra)
        {
            Result = ra;
            var settings = this.GetServiceLocator().ResolveType<ISettingsService>();
            
            ModeVariants = new Dictionary<ProgramModeEnum, string>
            {
                {ProgramModeEnum.Photogalery, "Загрузить в галерею"},
                {ProgramModeEnum.Medialibrary, "Загрузить в медибиблиотеку"},
                {ProgramModeEnum.Disk, "Сохранить на диск"}
            };
            ResizeVariants = new Dictionary<int, ImageSize>();
            ResizeVariants.AddRange(settings.SizeDict);
        }
        #endregion

        #region Properties

        #region Result property

        /// <summary>
        /// Gets or sets the Result value.
        /// </summary>
        public ResultArray Result
        {
            get => GetValue<ResultArray>(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        /// <summary>
        /// Result property data.
        /// </summary>
        public static readonly PropertyData ResultProperty = RegisterProperty("Result", typeof(ResultArray));

        #endregion
        
        #region SelectedProgramModeEnum property

        /// <summary>
        /// Gets or sets the SelectedProgramModeEnum value.
        /// </summary>
        public ProgramModeEnum SelectedMode
        {
            get => Result.ProgramModeEnum;
            set => Result.ProgramModeEnum = value;
        }

        #endregion

        #region IsUseWatermark property

        /// <summary>
        /// Gets or sets the IsUseWatermark value.
        /// </summary>
        public bool IsUseWatermark
        {
            get => Result.IsUseWatermark;
            set => Result.IsUseWatermark = value;
        }

       

        #endregion

        public ImageSize SelectedResizeMode
        {
            get => Result.ResizeResolution;
            set => Result.ResizeResolution = value;
        }

        #region ResizeVariants property

        /// <summary>
        /// Gets or sets the ResizeVariants value.
        /// </summary>
        public Dictionary<int, ImageSize> ResizeVariants
        {
            get => GetValue<Dictionary<int, ImageSize>>(ResizeVeriantsProperty);
            set => SetValue(ResizeVeriantsProperty, value);
        }

        /// <summary>
        /// ResizeVariants property data.
        /// </summary>
        public static readonly PropertyData ResizeVeriantsProperty = RegisterProperty("ResizeVariants", typeof(Dictionary<int, ImageSize>));

        #endregion

        #region ModeVariants property

        /// <summary>
        /// Gets or sets the ModeVariants value.
        /// </summary>
        public Dictionary<ProgramModeEnum, string> ModeVariants
        {
            get => GetValue<Dictionary<ProgramModeEnum, string>>(ModeProperty);
            set => SetValue(ModeProperty, value);
        }

        /// <summary>
        /// ModeVariants property data.
        /// </summary>
        public static readonly PropertyData ModeProperty = RegisterProperty("ModeVariants", typeof(Dictionary<ProgramModeEnum, string>));

        #endregion
        /// <summary>
        /// Gets the title of the view model.
        /// </summary>
        /// <value>The title.</value>
        public override string Title => "ViewModelName view model.";
        
        #endregion

        #region Commands

        // TODO: Register commands with the vmcommand or VmCommandWithCanExecute Code template.

        #endregion

        #region Methods

        #endregion
    }
}