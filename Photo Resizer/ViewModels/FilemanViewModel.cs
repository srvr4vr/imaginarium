﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Catel.Collections;
using Catel.Data;
using Catel.MVVM;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public class FilemanViewModel: ViewModelBase
    {
        #region FileSystemRoot property

        /// <summary>
        /// Gets or sets the FileSystemRoot value.
        /// </summary>
        public FileSystemEntityViewModel FileSystemRoot
        {
            get => GetValue<FileSystemEntityViewModel>(FileSystemRootProperty);
            set => SetValue(FileSystemRootProperty, value);
        }

        /// <summary>
        /// FileSystemRoot property data.
        /// </summary>
        public static readonly PropertyData FileSystemRootProperty = RegisterProperty("FileSystemRoot", typeof(FileSystemEntityViewModel));

        #endregion

        public FilemanViewModel(ResultArray ra)
        {
            Result = ra;
            Fill();
        }

        public FilemanViewModel()
        {
        }

        #region Result property

        /// <summary>
        /// Gets or sets the Result value.
        /// </summary>
        public ResultArray Result
        {
            get => GetValue<ResultArray>(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        /// <summary>
        /// Result property data.
        /// </summary>
        public static readonly PropertyData ResultProperty = RegisterProperty("Result", typeof(ResultArray));

        #endregion

        #region CreateFolder command

        private Command<FileSystemEntityViewModel> _createFolderCommand;

        /// <summary>
        /// Gets the CreateFolder command.
        /// </summary>
        public Command<FileSystemEntityViewModel> CreateFolderCommand =>
            _createFolderCommand ?? (_createFolderCommand = new Command<FileSystemEntityViewModel>(CreateFolder));

        /// <summary>
        /// Method to invoke when the CreateFolder command is executed.
        /// </summary>
        private static async void CreateFolder(FileSystemEntityViewModel folder)
        {
            var v = new NewFolderView { DataContext = new NewFolderViewModel(folder.FullName) };
            var resultt = await DialogHost.Show(v, "subDialoge");
            var result = resultt?.ToString();
            if (result == "False" || string.IsNullOrEmpty(result)) { return; }
            var fullPath = Path.Combine(folder.FullName, result);
            Directory.CreateDirectory(fullPath);
            folder.Childs.Add(new FileSystemEntityViewModel(result, fullPath, IconEnum.Folder));
        }

        #endregion

        #region SelectedItem property

        /// <summary>
        /// Gets or sets the SelectedItem value.
        /// </summary>
        public FileSystemEntityViewModel SelectedItem
        {
            get => GetValue<FileSystemEntityViewModel>(SelectedItemProperty);
            set
            {
                SetValue(SelectedItemProperty, value);
                Result.Target = value?.FullName;
            }
        }

        /// <summary>
        /// SelectedItem property data.
        /// </summary>
        public static readonly PropertyData SelectedItemProperty = RegisterProperty("SelectedItem", typeof(object));

        #endregion

        public void Fill()
        {
            var root = new FileSystemEntityViewModel();
            var drives = DriveInfo.GetDrives().Where(x =>
                x.DriveType == DriveType.Fixed || x.DriveType == DriveType.Removable ||
                x.DriveType == DriveType.Network);
            var disksVms = new List<FileSystemEntityViewModel>();
            foreach (var drive in drives)
            {
                IconEnum icon;
                // ReSharper disable once SwitchStatementMissingSomeCases
                switch (drive.DriveType)
                {
                    case DriveType.Removable:
                        icon = IconEnum.Sd;
                        break;
                 case DriveType.Network:
                        icon = IconEnum.ServerNetwork;
                        break;
                    default:
                        icon = IconEnum.Harddisk;
                        break;
                }
                disksVms.Add(new FileSystemEntityViewModel(drive.Name, drive.Name, icon));
            }
            FileSystemRoot = root;
            root.Childs.AddRange(disksVms);
        }
    }
}