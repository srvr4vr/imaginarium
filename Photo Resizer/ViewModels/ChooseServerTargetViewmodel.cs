﻿using System.Threading.Tasks;
using Catel.Data;
using Catel.MVVM;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Annotations;
using Photo_Resizer.Interfaces;
using Photo_Resizer.Models;
using Photo_Resizer.Tools;

namespace Photo_Resizer.ViewModels
{
    public class ChooseServerTargetViewModel: ViewModelBase
    {
        #region Result property

        /// <summary>
        /// Gets or sets the Result value.
        /// </summary>
        public ResultArray Result
        {
            get => GetValue<ResultArray>(ResultProperty);
            set => SetValue(ResultProperty, value);
        }

        /// <summary>
        /// Result property data.
        /// </summary>
        public static readonly PropertyData ResultProperty = RegisterProperty("Result", typeof(ResultArray));

        #endregion

        public ChooseServerTargetViewModel() { }
        private readonly ISettingsService _settings;
        private readonly IBitrix _bx;

        [UsedImplicitly]
        public ChooseServerTargetViewModel(ResultArray ra, ISettingsService settings, IBitrix bx)
        {
            Result = ra;
            _settings = settings;
            _bx = bx;
            Load();
        }

        #region SelectedItem property

        /// <summary>
        /// Gets or sets the SelectedItem value.
        /// </summary>
        public TreeItemViewModel SelectedItem
        {
            get => GetValue<TreeItemViewModel>(SelectedItemProperty);
            set { SetValue(SelectedItemProperty, value);
                Result.Target = value.Id.ToString();
            }
        }

        /// <summary>
        /// SelectedItem property data.
        /// </summary>
        public static readonly PropertyData SelectedItemProperty = RegisterProperty("SelectedItem", typeof(TreeItemViewModel));

        #endregion

        private async void Load()
        {
            TreeItems = Result.ProgramModeEnum == ProgramModeEnum.Medialibrary
                ? await LoadMediaLibrary()
                : await LoadPhotogalery()
                ?? new TreeItemViewModel();
        }

        #region Refresh command

        private Command _refreshCommand;

        /// <summary>
        /// Gets the Refresh command.
        /// </summary>
        public Command RefreshCommand => _refreshCommand ?? (_refreshCommand = new Command(Refresh));

        /// <summary>
        /// Method to invoke when the Refresh command is executed.
        /// </summary>
        private void Refresh()
        {
            _settings.Load();
            //_bx = this.GetServiceLocator().ResolveType<IBitrix>();
            Load();
        }

        #endregion

        #region TreeItems property

        /// <summary>
        /// Gets or sets the TreeItems value.
        /// </summary>
        public TreeItemViewModel TreeItems
        {
            get => GetValue<TreeItemViewModel>(TreeItemsProperty);
            set => SetValue(TreeItemsProperty, value);
        }

        /// <summary>
        /// TreeItems property data.
        /// </summary>
        public static readonly PropertyData TreeItemsProperty = RegisterProperty("TreeItems", typeof(TreeItemViewModel));

        #endregion

        #region CreateFolder command

        private Command<TreeItemViewModel> _createFolderCommand;

        /// <summary>
        /// Gets the CreateFolder command.
        /// </summary>
        public Command<TreeItemViewModel> CreateFolderCommand =>
            _createFolderCommand ?? (_createFolderCommand = new Command<TreeItemViewModel>(CreateFolder));

        /// <summary>
        /// Method to invoke when the CreateFolder command is executed.
        /// </summary>
        private async void CreateFolder(TreeItemViewModel item)
        {
            var v = new NewFolderView { DataContext = new NewFolderViewModel(item.Name) };
            var result = await DialogHost.Show(v, "subDialoge");

            if (result.ToString() == "False")
            {
                return;
            }

            var res = await CreateFolder(result.ToString(), item.Id.ToString());

            if (res.Result == "error")
            {
                return;
            }
            item.ChildList.Add(new TreeItemViewModel { Id = int.Parse(res.Code), Name = result.ToString() });
        }

        #endregion


        private async Task<OperationResult> CreateFolder(string name, string parentId)
        {
            return Result.ProgramModeEnum == ProgramModeEnum.Medialibrary
                ? await _bx.CreateMedialibraryFolder(name, parentId)
                : await _bx.CreatePhotogaleryFolder(name, parentId);
        }

        public async Task<TreeItemViewModel> LoadPhotogalery() => (await _bx.GetPhotogaleyList())?.GetTree();
        public async Task<TreeItemViewModel> LoadMediaLibrary() => (await _bx.GetMedialibList())?.GetTree();

        
    }
}
