﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Catel;
using Catel.Collections;
using Catel.Data;
using Catel.Messaging;
using Catel.MVVM;
using GongSolutions.Wpf.DragDrop;
using Photo_Resizer.Models;
using System.Deployment.Application;

namespace Photo_Resizer.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IDropTarget
    {
        private readonly IMessageMediator _messengerMediator;

        #region Version property

        /// <summary>
        /// Gets or sets the Version value.
        /// </summary>
        public string Version
        {
            get => GetValue<string>(VersionProperty);
            set => SetValue(VersionProperty, value);
        }

        /// <summary>
        /// Version property data.
        /// </summary>
        public static readonly PropertyData VersionProperty = RegisterProperty("Version", typeof(string));

        #endregion

        #region ImageList property

        /// <summary>
        /// Gets or sets the ImageList value.
        /// </summary>
        public FastObservableCollection<ImageViewModel> ImageList
        {
            get => GetValue<FastObservableCollection<ImageViewModel>>(ImageListProperty);
            set => SetValue(ImageListProperty, value);
        }

        /// <summary>
        /// ImageList property data.
        /// </summary>
        public static readonly PropertyData ImageListProperty = RegisterProperty("ImageList", typeof(ObservableCollection<ImageViewModel>));

        #endregion

        public bool IsControlVisability => ImageList.Any();

        public MainWindowViewModel()
        {
            
        }

        public MainWindowViewModel(IMessageMediator mediator)
        {
            Argument.IsNotNull(() => mediator);
            _messengerMediator = mediator;
            ImageList = new FastObservableCollection<ImageViewModel>();
            GoOnCommand =
                new Command(() => _messengerMediator.SendMessage( new ResultArray(ImageList.Select(x => x.Model).ToList()), "firstStep"),
                    OnResizeImagesCanExecute);
            _messengerMediator.Register<IEnumerable<ImageViewModel>>(this, AddImages, "openfolder");
            IsDialogeOpen = false;

            _messengerMediator.Register<string>(this, CloseDialoge, "closeDialoge");
            try
            {
                Version = ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (InvalidDeploymentException)
            {
                Version = string.Empty;
            }

        }

        public void CloseDialoge(string x)
        {
            IsDialogeOpen = false;
        }

        public Command GoOnCommand { get; private set; }

        private bool OnResizeImagesCanExecute() => ImageList != null && ImageList.Count>0;


        public bool IsDialogeOpen
        {
            get => GetValue<bool>(IsDialogeOpenProperty);
            set => SetValue(IsDialogeOpenProperty, value);
        }

        public static readonly PropertyData IsDialogeOpenProperty = RegisterProperty(nameof(IsDialogeOpen), typeof(bool));

        #region ShowOk command


        /*private async void ExecuteRunDialog()
        {
            //let's set up a little MVVM, cos that's what the cool kids are doing:
            var view = new MainDialogeView
            {
                DataContext = new MainDialogeViewModel(ImageList.ToList(), _messageService)
            };

            //IsDialogeOpen = true;
            //show the dialog
            var Result = await DialogHost.Show(view, "RootDialog", delegate (object sender, DialogOpenedEventArgs args){});
        }

        private Command _showOkCommand;
        */
        /// <summary>
        /// Gets the ShowOk command.
        /// </summary>
        //public Command ShowOkCommand => _showOkCommand ?? (_showOkCommand = new Command(ExecuteRunDialog));


        #endregion

        #region StartProcess command

        private Command _startProcessCommand;

        /// <summary>
        /// Gets the StartProcess command.
        /// </summary>
        public Command StartProcessCommand =>
            _startProcessCommand ?? (_startProcessCommand = new Command(StartProcess));

        /// <summary>
        /// Method to invoke when the StartProcess command is executed.
        /// </summary>
        private void StartProcess()
        {
            _messengerMediator.SendMessage(ImageList.ToList(), "go");
            SelectedSlide++;
        }

        #endregion

        public void DragOver(IDropInfo dropInfo)
        {
            dropInfo.Effects= GetDropEffect(dropInfo);
        }

        private static DragDropEffects GetDropEffect(IDropInfo dropInfo)
        {
            try
            {
                var dragFileList = ((DataObject)dropInfo.Data).GetFileDropList().Cast<string>();
                return dragFileList.Any(ImageModel.IsImage) ? DragDropEffects.Copy : DragDropEffects.None;
            }
            catch (Exception)
            {
                return DragDropEffects.None;
            }

        }

        #region Clear command

        private Command _clearCommand;

        /// <summary>
        /// Gets the ClearCommand command.
        /// </summary>
        public Command ClearCommand => _clearCommand ?? (_clearCommand = new Command(Clear));

        /// <summary>
        /// Method to invoke when the ClearCommand command is executed.
        /// </summary>
        private async void Clear()
        {
            _cancellationTokenSource.Cancel();
            await Task.Delay(50);
            ImageList.Clear();
            SelectedSlide = 0;
            LoadedImage = 0;
            RaisePropertyChanged(nameof(IsControlVisability));
            _cancellationTokenSource=new CancellationTokenSource();
        }

        #endregion

        #region Close command

        private Command _closeCommand;

        /// <summary>
        /// Gets the Close command.
        /// </summary>
        public Command CloseCommand => _closeCommand ?? (_closeCommand = new Command(Close));

        /// <summary>
        /// Method to invoke when the Close command is executed.
        /// </summary>
        private void Close()
        {
            Application.Current.Shutdown();
        }

        #endregion

        #region SelectedSlide property

        /// <summary>
        /// Gets or sets the SelectedSlide value.
        /// </summary>
        public int SelectedSlide
        {
            get => GetValue<int>(SelectedSlideProperty);
            set => SetValue(SelectedSlideProperty, value);
        }




        /// <summary>
        /// SelectedSlide property data.
        /// </summary>
        public static readonly PropertyData SelectedSlideProperty = RegisterProperty("SelectedSlide", typeof(int));

        #endregion

        #region AddImages command

        private Command<IEnumerable<ImageViewModel>> _addImagesCommand;

        /// <summary>
        /// Gets the AddImages command.
        /// </summary>
        public Command<IEnumerable<ImageViewModel>> AddImagesCommand =>
            _addImagesCommand ?? (_addImagesCommand = new Command<IEnumerable<ImageViewModel>>(AddImages));

        /// <summary>
        /// Method to invoke when the AddImages command is executed.
        /// </summary>
        private void AddImages(IEnumerable<ImageViewModel> imageList)
        {
            //IsStartCardFlipped = true;
            ImageList.AddRange(imageList);
            SelectedSlide = 1;
            RaisePropertyChanged(nameof(IsControlVisability));
            GoOnCommand.RaiseCanExecuteChanged();
            Load(_cancellationTokenSource.Token);
        }

        private CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

        #endregion

        public void Drop(IDropInfo dropInfo)
        {
            try
            {
                IsLoading = true;
                var dragFileList = ((DataObject) dropInfo.Data).GetFileDropList().Cast<string>();
                var fileList = dragFileList as IList<string> ?? dragFileList.ToList();
                dropInfo.Effects = fileList.Any(ImageModel.IsImage) ? DragDropEffects.Copy : DragDropEffects.None;
                if (dropInfo.Effects == DragDropEffects.Copy)
                {
                    AddImages(fileList.Where(ImageModel.IsImage)
                        .Select(s => new ImageViewModel(s)));
                }
            }
            catch (Exception)
            {
                Clear();
            }
            finally
            {
                IsLoading = false;
            }
        }


        #region Minimise command

        private Command _minimiseCommand;

        /// <summary>
        /// Gets the Minimise command.
        /// </summary>
        public Command MinimiseCommand => _minimiseCommand ?? (_minimiseCommand = new Command(Minimise));

        /// <summary>
        /// Method to invoke when the Minimise command is executed.
        /// </summary>
        private void Minimise()
        {
            _messengerMediator.SendMessage("min", "min");
        }

        #endregion
        #region LoadedImage property

        /// <summary>
        /// Gets or sets the LoadedImage value.
        /// </summary>
        public int LoadedImage
        {
            get => GetValue<int>(LoadedImageProperty);
            set => SetValue(LoadedImageProperty, value);
        }

        /// <summary>
        /// LoadedImage property data.
        /// </summary>
        public static readonly PropertyData LoadedImageProperty = RegisterProperty("LoadedImage", typeof(int));

        #endregion

        #region IsLoading property

        /// <summary>
        /// Gets or sets the IsLoading value.
        /// </summary>
        public bool IsLoading
        {
            get => GetValue<bool>(IsLoadingProperty);
            set => SetValue(IsLoadingProperty, value);
        }

        /// <summary>
        /// IsLoading property data.
        /// </summary>
        public static readonly PropertyData IsLoadingProperty = RegisterProperty("IsLoading", typeof(bool));

        #endregion

        private async void  Load(CancellationToken cancellationToken)
        {
            foreach (var imageViewModel in ImageList)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return;
                }
                await imageViewModel.UpdateImage();
                LoadedImage++;
            }
        }
 
    }
}