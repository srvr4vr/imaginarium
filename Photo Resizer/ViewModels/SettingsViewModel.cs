﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Catel.Data;
using Catel.MVVM;
using ImageResizerCore;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Models;
using Photo_Resizer.Views;

namespace Photo_Resizer.ViewModels
{
    public class SettingsViewModel: ViewModelBase
    {
        public SettingsViewModel() { }
        private readonly ISettingsService _settings;

        #region WatermarkOpacity property

        /// <summary>
        /// Gets or sets the WatermarkOpacity value.
        /// </summary>
        public int WatermarkOpacity
        {
            get => _settings.WatermarkOpacity;
            set => _settings.WatermarkOpacity = value;
        }

        /// <summary>
        /// WatermarkOpacity property data.
        /// </summary>
        public static readonly PropertyData WatermarkOpacityProperty = RegisterProperty("WatermarkOpacity", typeof(int));

        #endregion

        #region Login property
        /// <summary>
        /// Gets or sets the Login value.
        /// </summary>
        public string Login
        {
            get => _settings.Login;
            set => _settings.Login = value;
        }
        #endregion

        #region HostAdress property

        /// <summary>
        /// Gets or sets the HostAdress value.
        /// </summary>
        public string HostAdress
        {
            get => _settings.HostAdress;
            set => _settings.HostAdress = value;
        }

        /// <summary>
        /// HostAdress property data.
        /// </summary>
        public static readonly PropertyData HostAdressProperty = RegisterProperty("HostAdress", typeof(string));

        #endregion

        #region Password property
        /// <summary>
        /// Gets or sets the Login value.
        /// </summary>
        public string Password
        {
            get => _settings.Password;
            set => _settings.Password = value;
        }
        #endregion

        #region WatermarkText property

        /// <summary>
        /// Gets or sets the WatermarkText value.
        /// </summary>
        public string WatermarkText
        {
            get => _settings.WatermarkText;
            set => _settings.WatermarkText = value;
        }
        #endregion

        #region Sizes property

        /// <summary>
        /// Gets or sets the Sizes value.
        /// </summary>
        public ObservableCollection<KeyValuePair<int, ImageSize>> Sizes =>
            new ObservableCollection<KeyValuePair<int, ImageSize>>(_settings.SizeDict.Where(x=>!x.Value.IsZero()));
           
        #endregion

        #region Delete command

        private Command<KeyValuePair<int, ImageSize>> _deleteCommand;

        /// <summary>
        /// Gets the Delete command.
        /// </summary>
        public Command<KeyValuePair<int, ImageSize>> DeleteCommand =>
            _deleteCommand ?? (_deleteCommand = new Command<KeyValuePair<int, ImageSize>>(Delete));

        /// <summary>
        /// Method to invoke when the Delete command is executed.
        /// </summary>
        private void Delete(KeyValuePair<int, ImageSize> item)
        {
            _settings.SizeDict.Remove(item.Key);
            _settings.Save();
            RaisePropertyChanged(nameof(Sizes));
        }

        #endregion

        public SettingsViewModel(ISettingsService settings)
        {
            _settings = settings;
            RaisePropertyChanged(nameof(WatermarkOpacity));
        }

        #region OpenDialog command

        private Command _openDialogCommand;

        /// <summary>
        /// Gets the OpenDialog command.
        /// </summary>
        public Command OpenDialogCommand => _openDialogCommand ?? (_openDialogCommand = new Command(OpenDialog));

        /// <summary>
        /// Method to invoke when the OpenDialog command is executed.
        /// </summary>
        private async void OpenDialog()
        {
            var view = new SizeDialogView {DataContext = new SizeDialogViewModel()};
            var result = await DialogHost.Show(view, "settingsDialoge");
            if (result is KeyValuePair<int, ImageSize> s)
            {
                if (s.Key == -1)
                {
                    Add(s.Value);
                }
                else
                {
                    _settings.SizeDict[s.Key] = s.Value;
                    RaisePropertyChanged(nameof(Sizes));
                }
                //GoToUpload(r);
            }
            else
            {
                Debug.WriteLine("false");
            }
        }

        #endregion

        #region Add command

        private Command<ImageSize> _addCommand;

        /// <summary>
        /// Gets the Add command.
        /// </summary>
        public Command<ImageSize> AddCommand => _addCommand ?? (_addCommand = new Command<ImageSize>(Add));

        /// <summary>
        /// Method to invoke when the Add command is executed.
        /// </summary>
        private void Add(ImageSize size)
        {
            _settings.AddResolution(size);
            RaisePropertyChanged(nameof(Sizes));
            // TODO: Handle command logic here
        }

        #endregion

        #region Edite command

        private Command<KeyValuePair<int, ImageSize>> _editeCommand;

        /// <summary>
        /// Gets the Edite command.
        /// </summary>
        public Command<KeyValuePair<int, ImageSize>> EditeCommand => _editeCommand ?? (_editeCommand = new Command<KeyValuePair<int, ImageSize>>(Edit));

        /// <summary>
        /// Method to invoke when the Edite command is executed.
        /// </summary>
        private async void Edit(KeyValuePair<int, ImageSize> pair)
        {
            var view = new SizeDialogView { DataContext = new SizeDialogViewModel(pair) };
            var result = await DialogHost.Show(view, "settingsDialoge");
            if (result is KeyValuePair<int, ImageSize> s)
            {
                if (s.Key == -1)
                {
                    Add(s.Value);
                }
                else
                {
                    _settings.SizeDict[s.Key] = s.Value;
                    RaisePropertyChanged(nameof(Sizes));
                }
                //GoToUpload(r);
            }
            else
            {
                Debug.WriteLine("false");
            }
        }

        #endregion
    }
}