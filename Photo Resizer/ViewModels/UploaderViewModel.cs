﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Catel.IoC;
using ImageResizerCore;
using MaterialDesignThemes.Wpf;
using Newtonsoft.Json;
using Photo_Resizer.Interfaces;
using Photo_Resizer.Models;

namespace Photo_Resizer.ViewModels
{
    public sealed class UploaderViewModel: UploadBaseViewModel
    {
        private readonly IBitrix _bx;
        public UploaderViewModel(ResultArray ra, ISettingsService settings, IBitrix bitrix): base(ra, settings)
        {
            _bx = bitrix;//this.GetDependencyResolver().Resolve<IBitrix>();
            Start(Cts.Token);
        }

        public override async void Start(CancellationToken cancellationToken)
        {
            await Task.Delay(1000, cancellationToken);
            try
            {
                var resultList = new List<string>();
                foreach (var img in Result.ImageList)
                {
                    if (cancellationToken.IsCancellationRequested)
                        return;
                    using (var bmp = Result.IsUseWatermark
                        // ? img.GetResizedBitmap(Result.ResizeResolution, Settings.WatermarkText, (int)(Settings.WatermarkOpacity * 2.55))
                        //: img.GetResizedBitmap(Result.ResizeResolution))
                        ? img.GetResizedBitmap(Properties.Resources.watermark)
                        : img.GetResizedBitmap(new ImageSize(1500, 0)))
                    {
                        var result = Result.ProgramModeEnum == ProgramModeEnum.Medialibrary
                            ? await _bx.UploadToMedialib64(Result.Target, img.Name, bmp)
                            : await _bx.UploadToPhotogalery64(Result.Target, img.Name, bmp);
                        resultList.Add(result);
                        Count++;
                    }
                }
                if (Result.ProgramModeEnum == ProgramModeEnum.Medialibrary)
                {
                    var resMl = resultList.Select(JsonConvert.DeserializeObject<MediaLibResult>);
                    DialogHost.CloseDialogCommand.Execute(resMl.ToList(), null);
                }
                else
                {
                    DialogHost.CloseDialogCommand.Execute(resultList, null);
                }
            }
            catch 
            {

            }
        }
    }
}