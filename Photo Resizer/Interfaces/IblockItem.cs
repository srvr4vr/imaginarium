﻿using Newtonsoft.Json;

namespace Photo_Resizer.Interfaces
{
    public class IblockItem : IBitrixItem
    {

        [JsonProperty("ID")]
        public string IdString { get; set; }

        public int Id => int.Parse(IdString);

        [JsonProperty("NAME")]
        public string Name { get; set; }

        [JsonProperty("IBLOCK_SECTION_ID")]
        private string _parentIdString;

        public string ParentIdString
        {
            get => _parentIdString;
            set => _parentIdString = string.IsNullOrWhiteSpace(value) ? "0" : value;
        }

        public int ParentId
        {
            get => string.IsNullOrWhiteSpace(ParentIdString) ? 0 : int.Parse(ParentIdString);
            set => ParentIdString = value.ToString();
        }

        public IblockItem()
        {
            ParentIdString = "0";
            ParentId = 0;
        }

        public override string ToString() => $"[{Id}]: {Name} родитель {ParentId}";
    }
}