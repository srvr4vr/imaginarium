﻿using System.Collections.Generic;
using ImageResizerCore;

namespace Photo_Resizer.Models
{
    public interface ISettingsService
    {
        IDictionary<int, ImageSize> SizeDict { get; }
        void AddResolution(ImageSize imageSize);
        void Load();
        void Save();
        string HostAdress { get; set; }
        string Login { get; set; }
        string Password { get; set; }
        string WatermarkText { get; set; }
        int WatermarkOpacity { get; set; }
    }
}