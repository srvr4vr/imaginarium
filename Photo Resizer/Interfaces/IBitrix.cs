﻿using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Photo_Resizer.Models;

namespace Photo_Resizer.Interfaces
{
    public interface IBitrix
    {
        Task<string> UploadToPhotogalery64(string id, string fileName, Bitmap image);
        Task<string> UploadToMedialib64(string id, string fileName, Bitmap image);
        Task<List<ServerItem>> GetMedialibList();
        Task<List<ServerItem>> GetPhotogaleyList();
        Task<OperationResult> CreateMedialibraryFolder(string name, string parentId);
        Task<OperationResult> CreatePhotogaleryFolder(string name, string parentId);
    }
}