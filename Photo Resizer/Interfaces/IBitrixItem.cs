﻿namespace Photo_Resizer.Interfaces
{
    public interface IBitrixItem
    {
        string IdString { get; set; }
        int Id { get; }
        string Name { get; set; }
        string ParentIdString { get; set; }
        int ParentId { get; set; }
        string ToString();
    }
}