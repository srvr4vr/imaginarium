﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Photo_Resizer.Models;
using Photo_Resizer.ViewModels;

namespace Photo_Resizer.Tools
{
    public static class Helpers
    {
        public static TreeItemViewModel GetTree(this List<ServerItem> list)
        {
            if (list.Count <1) return new TreeItemViewModel();
            var rootId = list.Min(x => x.ParentId);
            var root = new TreeItemViewModel { Name = "root", Id = rootId }; 
            return GetTree2(list, root);
        }

        public static int GetImageCount(this FileSystemEntityViewModel fileSystemEntityViewModel)
        {
            try
            {
                return Directory.GetFiles(fileSystemEntityViewModel.FullName).Count(ImageModel.IsImage);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        private static TreeItemViewModel GetTree2(IReadOnlyCollection<ServerItem> list, TreeItemViewModel root)
        {
            if (list.Count < 2)
            {
                return new TreeItemViewModel(list.FirstOrDefault());
            }

            var children = list.Where(x => x.ParentId == root.Id).ToList();

            foreach (var item in children)
            {
                var newlist = list.Except(children).ToList();
                var child = GetTree2(newlist, new TreeItemViewModel(item));
                root.ChildList.Add(child);
            }
            return root;
        }
    }
}