﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Catel.IoC;
using Catel.Messaging;
using MaterialDesignThemes.Wpf;
using Photo_Resizer.Models;
using Photo_Resizer.ViewModels;

namespace Photo_Resizer.Views
{
    /// <inheritdoc>
    ///     <cref></cref>
    /// </inheritdoc>
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public MainWindow()
        {
            InitializeComponent();
            var messanger = this.GetServiceLocator().ResolveType<IMessageMediator>();
            messanger.Register<ResultArray>(this, ShowChooseWay, "firstStep");
            messanger.Register<string>(this,x=>
            {
                WindowState = WindowState.Minimized;
            },"min");

        }


        private void WindowControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private async void ShowChooseWay(ResultArray resultArray)
        {
            var view = new ChooseWayView {DataContext = new ChooseWayViewModel(resultArray)};
            var x =await MainDialogHost.ShowDialog(view);
            if (x is ResultArray r)
            {
                ShowSecondScreen((ResultArray)r.Clone());
            }
            else
            {
                Debug.WriteLine("false");
            }
        }

        private async void ShowSecondScreen(ResultArray resultArray)
        {
            object view;
            switch (resultArray.ProgramModeEnum)
            {
                case ProgramModeEnum.Medialibrary:
                case ProgramModeEnum.Photogalery:
                    view = new ChooseMLfolderView
                    {
                        //DataContext = new ChooseServerTargetViewmodel(resultArray)
                        DataContext =
                            TypeFactory.Default
                                .CreateInstanceWithParametersAndAutoCompletion<ChooseServerTargetViewModel>(resultArray)
                    };
                    break;

                default:
                    view = new FilemanView {DataContext = new FilemanViewModel(resultArray)};
                    break;
            }

            var result = await MainDialogHost.ShowDialog(view);

            if (result is ResultArray r)
            {
                GoToUpload((ResultArray)r.Clone());
            }
            
            else
            {
                Debug.WriteLine("false");
            }
        }

        private async void GoToUpload(ResultArray resultArray)
        {
            object view;
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (resultArray.ProgramModeEnum)
            {
                case ProgramModeEnum.Medialibrary:
                case ProgramModeEnum.Photogalery:
                    var dc = TypeFactory.Default
                        .CreateInstanceWithParametersAndAutoCompletion<UploaderViewModel>(
                            resultArray.Clone()); //new UploaderViewModel((ResultArray)resultArray.Clone());
                    view = new UploaderView {DataContext = dc};
                    break;

                default:
                    var dcf = TypeFactory.Default
                        .CreateInstanceWithParametersAndAutoCompletion<SaveFileToDiskViewModel>(
                            resultArray.Clone()); //new SaveFileToDiskViewModel((ResultArray) resultArray.Clone());
                    view = new UploaderView { DataContext = dcf };
                    break;
            }
            
            var x =await MainDialogHost.ShowDialog(view);
            if (!(x is List<MediaLibResult> mlr)) return;
            view = new HtmlResultView { DataContext = new HtmlResultViewModel(mlr) };

            await MainDialogHost.ShowDialog(view);
        }
    }
}
