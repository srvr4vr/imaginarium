﻿using System.Windows;
using Catel.IoC;
using Catel.Logging;
using ImageResizerCore;
using Photo_Resizer.Interfaces;
using Photo_Resizer.Models;

namespace Photo_Resizer
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Raises the <see cref="E:System.Windows.Application.Startup"/> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.StartupEventArgs"/> that contains the event data.</param>
        protected override void OnStartup(StartupEventArgs e)
        {
#if DEBUG
            LogManager.AddDebugListener();
#endif
            var serviceLocator = ServiceLocator.Default;

            ServiceLocator.Default.RegisterType<ISettingsService, Settings>(RegistrationType.Singleton);

            ServiceLocator.Default.RegisterType<IBitrix, Bitrix>(RegistrationType.Transient);

            ServiceLocator.Default.RegisterType<IResizeCalculator, ResizeCalculator>();
            ServiceLocator.Default.RegisterType<IImageResizer, ImageResizer>();

            var settings = serviceLocator.GetDependencyResolver().Resolve<ISettingsService>();//.ResolveType(typeof(ISettingsService));
            settings.Load();
            // Note: use Orchestra.Core to enable StyleHelper
            // StyleHelper.CreateStyleForwardersForDefaultStyles();

            //serviceLocator.RegisterType<IFamilyService, FamilyService>();

            //var uiVisualizerService = serviceLocator.ResolveType<IUIVisualizerService>();
            //uiVisualizerService.Register(typeof(PersonViewModel), typeof(PersonWindow));

            base.OnStartup(e);
        }
    }
}
