﻿namespace ImageResizerCore
{
    public interface IResizeCalculator
    {
        ImageSize CalculateImageSize(ImageSize originImageSize, ImageSize desiredImageSize);
    }
}