﻿using System.Drawing;

namespace ImageResizerCore
{
    public interface IImageResizer
    {
        Bitmap ResizeImage(Image image, ImageSize imageSize);
        Bitmap ResizeImage(string imagePath, ImageSize imageSize);
    }
}