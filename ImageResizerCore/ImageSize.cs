﻿using System;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global
// ReSharper disable NonReadonlyMemberInGetHashCode

namespace ImageResizerCore
{
    public class ImageSize : IEquatable<ImageSize>
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public string Discription { get; set; }


        public ImageSize(int width, int height, string discription)
        {
            Height = height;
            Width = width;
            Discription = discription;
        }

        private static string GetDiscription(int width, int height)
        {
            string result;
            if (width == 0)
            {
                result = $"По высоте {height}";
            }
            else if (height == 0)
            {
                result = $"По ширине {width}";
            }
            else result = $"{width}x{height}";
            return result;
        }
        

        public ImageSize(int width, int height) : this(width, height, GetDiscription(width, height))
        {
        }

        public ImageSize()
        {
            
        }

        public bool Equals(ImageSize other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Height == other.Height && Width == other.Width;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ImageSize)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Height * 397) ^ Width;
            }
        }

        public bool IsOneSideZero() => Height == 0 || Width == 0;

        public bool IsZero() => Height == 0 && Width == 0;

        public override string ToString() => Discription;
    }
}