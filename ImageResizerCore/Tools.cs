﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace ImageResizerCore
{
    public static class Tools
    {
        public static Bitmap StampWatermarkDiagonal(this Bitmap btmp, string text, int alpha)
        {
            using (var graphics = Graphics.FromImage(btmp))
            {
                var tangent = btmp.Height / (double)btmp.Width;

                var angle = Math.Atan(tangent) * (180 / Math.PI);
                graphics.RotateTransform((float)angle);

                var halfHypotenuse = Math.Sqrt(btmp.Height
                                               * btmp.Height +
                                               btmp.Width *
                                               btmp.Width) / 2;

                using (var stringFormat = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Center
                })
                {
                    using (Brush brush = new SolidBrush(Color.FromArgb(alpha, 0, 0, 0)))
                    {
                        var font = CalculateFontSize(btmp.Width, btmp.Height, graphics, angle, text);
                        graphics.DrawString(text, font, brush, new Point((int)halfHypotenuse, 0), stringFormat);
                    }
                }
            }
            return btmp;
        }

        public static Bitmap StampWatermarkHorizontal(this Bitmap btmp, string text, int alpha)
        {
            using (var graphics = Graphics.FromImage(btmp))
            {

                using (var stringFormat = new StringFormat
                {
                    Alignment = StringAlignment.Center,
                    LineAlignment = StringAlignment.Near
                })
                {
                    using (Brush brush = new SolidBrush(Color.FromArgb(alpha, 0, 0, 0)))
                    {
                        var font = CalculateFontSize(btmp.Width, btmp.Height, graphics, text);
                        var sizef = graphics.MeasureString(text, font, int.MaxValue);
                        graphics.DrawString(text, font, brush, new Point(btmp.Width/2, btmp.Height-(int)sizef.Height), stringFormat);
                    }
                }
            }
            return btmp;
        }

        private static Font CalculateFontSize(int width, int height, Graphics g, double angle, string text)
        {
            var font = default(Font);
            for (var i = 800; i > 0; i--)
            {
                font = new Font(FontFamily.GenericSansSerif, i, FontStyle.Regular);
                var sizef = g.MeasureString(text, font, int.MaxValue);

                var sin = Math.Sin(angle * (Math.PI / 180));
                var cos = Math.Cos(angle * (Math.PI / 180));

                var opp1 = sin * sizef.Width;
                var adj1 = cos * sizef.Height;

                var opp2 = sin * sizef.Height;
                var adj2 = cos * sizef.Width;

                if (opp1 + adj1 < height &&
                    opp2 + adj2 < width)
                {
                    break;
                }
            }
            return font;
        }

        private static Font CalculateFontSize(int width, int height, Graphics g, string text)
        {
            var font = default(Font);
            for (var i = 10000; i > 0; i-=10)
            {
                font = new Font(FontFamily.GenericSansSerif, i, FontStyle.Regular);
                var sizef = g.MeasureString(text, font, int.MaxValue);

                if (sizef.Width < width && sizef.Height < height)  break;

            }
            return font;
        }

        public static Bitmap StampWatermark(this Bitmap btmp, Bitmap watermark)
        {

            try
            {
                using (Graphics g = Graphics.FromImage(btmp))
                {
                    g.DrawImage(watermark, new Rectangle(0, btmp.Height - watermark.Height, watermark.Width, watermark.Height));
                }

                return btmp;
            }
            catch (Exception)
            {
                if (btmp != null) btmp.Dispose();
                throw;
            }
            finally
            {
                watermark.Dispose();
            }
        }
    }
}