﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;

namespace ImageResizerCore
{
    public class ImageResizer : IImageResizer
    {
        public static ImageCodecInfo GetJpegCodecInfo()
        {
            var codecs = ImageCodecInfo.GetImageDecoders();
            return codecs.FirstOrDefault(codec => codec.FormatID == ImageFormat.Jpeg.Guid);
        }

        public static EncoderParameters GetJpegEncoderParams(long quality)
        {
            var jpgEncoder =Encoder.Quality;
            var jpgEncoderParameters = new EncoderParameters(1);
            var qualityEncoderParameter = new EncoderParameter(jpgEncoder, quality);
            jpgEncoderParameters.Param[0] = qualityEncoderParameter;
            return jpgEncoderParameters;
        }

        private readonly IResizeCalculator _calculator;

        public ImageResizer(IResizeCalculator calculator)
        {
            _calculator = calculator;
        }

        public Bitmap ResizeImage(Image image, ImageSize imageSize)
        {
            var originalSize = new ImageSize(image.Width, image.Height);

            var desireSize = _calculator.CalculateImageSize(originalSize, imageSize);

            var destRect = new Rectangle(0, 0, desireSize.Width, desireSize.Height);
            var destImage = new Bitmap(desireSize.Width, desireSize.Height, PixelFormat.Format32bppPArgb);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return destImage;
        }

        public Bitmap ResizeImage(string imagePath, ImageSize imageSize)
        {
            var image = Image.FromFile(imagePath);
            return ResizeImage(image, imageSize);
        }
    }
}
