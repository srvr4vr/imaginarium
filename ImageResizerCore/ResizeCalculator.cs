﻿using System;

namespace ImageResizerCore
{
    public class ResizeCalculator : IResizeCalculator
    {
        public ImageSize CalculateImageSize(ImageSize originImageSize, ImageSize desiredImageSize)
        {
            if (desiredImageSize.IsZero()) return originImageSize;
            return desiredImageSize.IsOneSideZero()
                ? CalculateByOneSide(originImageSize, desiredImageSize)
                : CalculateConservative(originImageSize, desiredImageSize);
        }

        private static ImageSize CalculateConservative(ImageSize originImageSize, ImageSize desiredImageSize)
        {
            var xFactor = CalculateRatio(originImageSize);
            var dFactor = CalculateRatio(desiredImageSize);

            return Math.Abs(xFactor - dFactor) < 0.005
                ? desiredImageSize
                : xFactor > dFactor
                    ? CalculateByWidth(originImageSize, desiredImageSize)
                    : CalculateByHeight(originImageSize, desiredImageSize);
        }

        private static ImageSize CalculateByOneSide(ImageSize originImageSize, ImageSize desiredImageSize) =>
            desiredImageSize.Height != 0
                ? CalculateByHeight(originImageSize, desiredImageSize)
                : CalculateByWidth(originImageSize, desiredImageSize);

        private static ImageSize CalculateByHeight(ImageSize originImageSize, ImageSize desiredImageSize)
        {
            var height = desiredImageSize.Height;
            var deltaHeight = height / (double)originImageSize.Height;
            var width = (int)(originImageSize.Width * deltaHeight);
            return new ImageSize(width, height);
        }

        private static ImageSize CalculateByWidth(ImageSize originImageSize, ImageSize desiredImageSize)
        {
            var width = desiredImageSize.Width;
            var deltaWidth = width / (double)originImageSize.Width;
            var height = (int)(originImageSize.Height * deltaWidth);
            return new ImageSize(width, height);
        }

        private static double CalculateRatio(ImageSize imageSize) => imageSize.Width / (double)imageSize.Height;
    }
}