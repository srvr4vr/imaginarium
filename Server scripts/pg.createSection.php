<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$id= $_GET['id'];
$name = iconv("UTF-8", "WINDOWS-1251",  $_GET['name']);


echo $Id;
echo $Name;
if (empty($name)) 
{
	echo '{result: "error", code: "wrong Name"}';
	return;
}

if (empty($id)) 
{
	echo '{result: "error", code: "wrong Id"}';
	return;
}

$bs = new CIBlockSection;
$arFields = Array(
  "ACTIVE" => true,
  "IBLOCK_SECTION_ID" => $id,
  "IBLOCK_ID" => 13,
  "NAME" => $name,

  "DESCRIPTION" => $name,
  );


$ID = $bs->Add($arFields);
$res = ($ID>0);


if(!$res)
  echo '{result: "error", code: '. $bs->LAST_ERROR.'}';
else 
  echo '{result: "success", code: '. $ID.'}';
?>