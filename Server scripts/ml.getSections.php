<?
require "safejson.php";


require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("fileman");

CMedialib::Init();


$arCollections = CMedialibCollection::GetList(array('arOrder'=>Array('NAME'=>'ASC'),'arFilter' => array('ACTIVE' => 'Y')));

$idArray = Array();
foreach($arCollections as $item)
{
	$idArray2 = Array('ID'=>$item['ID'],'NAME'=> $item['NAME'],'PARENT_ID'=>$item['PARENT_ID']);
	array_push($idArray,$idArray2);
}


echo json_safe($idArray);


?>