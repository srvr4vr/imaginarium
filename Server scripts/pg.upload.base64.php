<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
require "safejson.php";

try
{
	$id = $_GET['id'];

	$el = new CIBlockElement;
	$name = $_GET['name'];
	$upload = $_SERVER['DOCUMENT_ROOT']."/upload";

	$dateTime = new DateTime();
	$date =MakeTimeStamp($dateTime, "DD.MM.YYYY HH:MI:SS");

	if (count($_POST))
	{
		$simName = iconv('utf-8', 'windows-1251', $name);

		$arTParams = array("replace_space"=>"_", "safe_chars" => ".");
		$transName =  CUtil::translit($simName,"ru",$arTParams);

		$tfile= $upload.'/'.$transName;
		$prewiewfile = $upload.'/prew_'.$transName;
		$simName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $simName);
		$simName = preg_replace('/\\(\\d+\\)/', '', $simName);
		$imageData = base64_decode($_POST['myImageData']);

		$putfile_res= file_put_contents($tfile, $imageData, LOCK_EX);


		$file = CFile::MakeFileArray( $tfile);


		$prewiew = CFile::ResizeImageFile( // ���������� �������� ��� ������
            $sourceFile = $tfile ,
            $destinationFile = $prewiewfile,
            $arSize = array('width'=>300,'height'=>200),
            $resizeType = BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
            $arWaterMark = array(),
            $jpgQuality=false,
            $arFilters =false
        );

		$prewiewAr = CFile::MakeFileArray( $prewiewfile);

		$arLoadProductArray = Array(
			"MODIFIED_BY"    => 8222, // ������� ������� ������� ������������� 8222
			"IBLOCK_SECTION_ID" => $id,          // ������� ����� � ����� �������
			"IBLOCK_ID"      => 13,
			"NAME"           => $simName,
			"DATE"			=>$date,
			"PROPERTY_VALUES" => array("REAL_PICTURE" => $file, "DATE"=>$date ),
			"ACTIVE"         => "Y",            // �������
			"DETAIL_PICTURE" => $file,
			"PREVIEW_PICTURE"=> $prewiewAr
		);

		$result= ($PRODUCT_ID = $el->Add($arLoadProductArray))? 
			'{result:"success", code: "'.$PRODUCT_ID.'"}':
		    '{result:"error", code: "'.$el->LAST_ERROR.'"}';
			
		echo $result;
	}
}
catch (Exception $e)
{
	echo '{result:"error", code: "'.$e->getMessage().'"}';
}
finally
{
	unlink($tfile);
	unlink($prewiewfile);
}
?>