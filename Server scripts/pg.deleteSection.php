<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");
$id = $_GET['id'];

if (empty($id)) 
{
	echo '{result: "error", code: "wrong Id"}';
	return;
}

if(CIBlockSection::Delete($id))
{
  echo '{result: "success", code: '. $id.'}';
}
else
{
	echo '{result: "error", code: "something wrong. oh"}';
}
?>