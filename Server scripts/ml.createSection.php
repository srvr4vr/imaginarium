<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("fileman");
CMedialib::Init();

try
{
$Name = $_GET['name'];
$Id = $_GET['id'] ;

if (empty($Name)) 
{
	echo '{result: "error", code: "wrong Name"}';
	return;
}

if (empty($Id)) 
{
	echo '{result: "error", code: "wrong Id"}';
	return;
}


$arFields = Array (
   "arFields" => 
   Array (
      "ID" => 0, // ID ��������� ��� ����������, 0 ��� ���������� �����
      "NAME" => $Name, // ��������
      "DESCRIPTION" => $Name, // ��������
      "OWNER_ID" => "8222", // ID ������������
      "PARENT_ID" => $Id, // ID ������������ ���������
      "ACTIVE" => "Y", // ����������
      "ML_TYPE" => "1", // ��� ���������, �� ���������: 1 - �����������, 2 - �����, 3 - �����
   ),
);

$COLLECTION_ID = CMedialibCollection::Edit($arFields); // ������� ���������� ID ���������

echo '{result: "success", code: '.$COLLECTION_ID.'}';
}

catch (Exception $e) {
	echo '{result: "error", code: '.$e->getMessage().'}';
}
?> 